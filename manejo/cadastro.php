<?php

include_once '../calender/components/header.php';

include_once '../calender/components/footer.php';

?>

<!-- JQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>


<div class="container-sm card border border-dark" style="width: 28rem;">

    <br>
    <h3 class="text-center">Cadastro de novos Usuários</h3>
    <br>

    <form id="inline" method="post" action="php_action/db_cadastro.php" class="">
        <div class="form-label-group">
            <input type="text" class="form-control border border-dark" placeholder="Nome" id="nome" name="nome">
            <label for="nome"></label>
        </div>
        <div class="form-label-group">
            <input type="text" class="form-control border border-dark" placeholder="Email" id="email" name="email">
            <label for="email"></label>
        </div>
        <div class="form-label-group">
            <input type="tel" class="telefone form-control border border-dark" placeholder="Telefone celular" id="tel" name="tel">
            <label for="tel"></label>
        </div>

        <div class="form-label-group">
            <input type="password" class="form-control border border-dark" placeholder="Senha" id="senha" name="senha">
            <label for="senha"></label>
        </div>
        <div class="form-label-group">
            <input type="password" class="form-control border border-dark" placeholder="Confirmar senha" id="senharep" name="senharep">
            <label for="senharep"></label>
        </div>

        <button type="submit" name="btn-cadastrar" class="btn-success col border border-dark">Cadastrar</button>
    </form>
</div>

<script type="text/javascript">
    jQuery("input.telefone")
        .mask("(99) 9999-9999?9")
        .focusout(function(event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });
</script>