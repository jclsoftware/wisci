<?php

    include_once '../calender/components/header.php';

    include_once '../calender/components/footer.php';


?>


<div class="container-sm card border border-dark" style="width: 28rem;">

        <br>
        <h3 class="text-center">Login de Usuário</h3>
        <br>

        <form id="inline" method="post" action="" class="">
            <div class="form-label-group">
                <input type="text" class="form-control border border-dark" placeholder="Email" id="email" name="email">
                <label for="email"></label>
            </div>

            <br>
            
            <div class="form-label-group">
                <input type="password" class="form-control border border-dark" placeholder="Senha" id="senha" name="senha">
                <label for="senha"></label>
            </div>

            <br>

            <button type="submit" class="btn btn-success col border border-dark">Entrar</button>

            <br>
            <br>

            <p class="text-right">
            Novo aqui ?<br>
                <a href="cadastro.php"> Realize Cadastro</a>
            </p>
        </form>
    </div>


