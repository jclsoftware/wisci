
 <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModal" style="padding: 10px">
	<i class="fa fa-user" aria-hidden="true"></i>
	PERFIL
</button>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Seus dados</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>

					<table class="table">
						<tbody>
							<tr>
								<th scope="row">Nome</th>
								<td><?php echo $_SESSION['nome']; ?></td>
								<td><i class="fas fa-edit"></td>
								</tr>
								<tr>
									<th scope="row">Email</th>
									<td>gabriel@gmail.com</td>
									<td><i class="fas fa-edit"></td>
									</tr>
									<tr>

										<tr>
									<th scope="row">Telefone</th>
									<td>(33) 99999-9999</td>
									<td><i class="fas fa-edit"></td>
									</tr>
									<tr>
										<th scope="row">Senha</th>
										<td>********</td>
										<td><i class="fas fa-edit"></i></td>
									</tr>
								</tbody>
							</table>


							<br>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary">Fechar</button>
					</div>
				</div>
			</div>
		</div>