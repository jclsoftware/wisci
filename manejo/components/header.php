<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">




<html lang="pt-br">
        
<head>
    <meta charset="utf-8">
    <title>Calendário de Manejos</title>
    <meta name="generator" content="LibreOffice 6.3.2.2 (Linux)" />
    <meta name="author" content="vanessa.felipe" />
    <meta name="created" content="2016-01-19T19:09:08" />
    <meta name="changed" content="2020-09-15T19:44:52" />
    <meta name="AppVersion" content="16.0300" />
    <meta name="Company" content="Microsoft" />
    <meta name="DocSecurity" content="0" />
    <meta name="HyperlinksChanged" content="false" />
    <meta name="LinksUpToDate" content="false" />
    <meta name="ScaleCrop" content="false" />
    <meta name="ShareDoc" content="false" />

    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <!-- icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">


    <style type="text/css">
        body {



            font-family: 'Ubuntu', sans-serif;
        }

        .calendario {
            font-family: "Ubuntu";
            font-size: 14;
            font-weight: bold;
        }

        a.comment-indicator:hover+comment {
            background: #ffd;
            position: absolute;
            display: block;
            border: 1px solid black;
            padding: 0.5em;
        }

        a.comment-indicator {
            background: red;
            display: inline-block;
            border: 1px solid black;
            width: 0.5em;
            height: 0.5em;
        }

        comment {
            display: none;
        }

        .fixar {
            width: 100%;
            height: 30px;
            margin: auto;
            bottom: 0;
            position: fixed;
            text-align: center;

            color: white;

            background: #353a40;
        }
    </style>

    <!-- FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,500;1,400&display=swap" rel="stylesheet">

    <!-- CSS -->
    <style>
        body {



            font-family: 'Ubuntu', sans-serif;

        }
    </style>

</head>

<body>

    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
        <a class="navbar-brand" href="paginicial.php">Calendário de Manejos Reprodutivo, Sanitário e Zootécnico</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link text-success" href="#">Orientações gerais</a>
                </li>

            </ul>



            

            <button type="button" class="btn btn-outline-danger my-2 my-sm-0" data-toggle="modal" data-target="#exampleModal" style="padding: 10px">
                <i class="fa fa-user" aria-hidden="true"></i>
                PERFIL
            </button>


            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Seus dados</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>

                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Nome</th>
                                            <td><?php echo $_SESSION['nome']; ?></td>
                                            <td><i class="fas fa-edit"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Email</th>
                                                <td>gabriel@gmail.com</td>
                                                <td><i class="fas fa-edit"></td>
                                                </tr>
                                                <tr>

                                                    <tr>
                                                        <th scope="row">Telefone</th>
                                                        <td>(33) 99999-9999</td>
                                                        <td><i class="fas fa-edit"></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Senha</th>
                                                            <td>********</td>
                                                            <td><i class="fas fa-edit"></i></td>
                                                        </tr>
                                                    </tbody>
                                                </table>


                                                <br>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary">Fechar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </nav>
                    <br>

                    <body>
                        